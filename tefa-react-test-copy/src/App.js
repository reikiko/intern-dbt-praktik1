import "./App.css";
import React from "react";

class Tools extends React.Component {
  constructor(props) {
    super();

    this.state = {
      text: [],
    };
    this.upperFirst = this.upperFirst.bind(this);
    this.reverseText = this.reverseText.bind(this);
  }
  upperFirst() {
    if (this.nameTextInput !== null) {
      let string = this.nameTextInput.value;
      function capitalizeFirstLetters(str) {
        return str.toLowerCase().replace(/^\w|\s\w/g, function (letter) {
          return letter.toUpperCase();
        });
      }

      this.setState({
        text: capitalizeFirstLetters(string),
      });
    }
  }
  reverseText() {
    if (this.nameTextInput !== null) {
      this.setState({
        text: this.nameTextInput.value.split("").reverse().join(""),
      });
    }
  }
}

function App() {
  return (
    <div id="box">
      <div class="item">
        <h1>Input :</h1>
        <form action="">
          <textarea type="text"></textarea>
        </form>
        <button type="submit">Convert</button>
      </div>
      <div class="item">
        <h1>Output :</h1>
      </div>
    </div>
  );
}

export default App;
