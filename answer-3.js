class World {
  constructor(num) {
    // membuat atribut cities yang isinya berupa array dari object City
    // yang berjumlah sesuai dengan value num
    this.cities = [num];
  }

  add_city(name) {
    City = new City(name);
  }

  random_city() {
    return this.cities[Math.random()];
  }

  total_cities() {
    return cities.length;
  }
}

class City {
  constructor(name) {
    if (name !== undefined) {
      this.name = name;
    } else {
      const characters =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      function generateString(length) {
        let result = " ";
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
          result += characters.charAt(
            Math.floor(Math.random() * charactersLength)
          );
        }

        return result;
      }
      this.name = generateString(5);
    }
  }

  add_citizen() {
    citizen = new Citizen();
  }
}

class Citizen {
  constructor() {
    age = math.random();
  }
}

const world = new World(50);
world.add_city("Jakarta");
console.log("Random city name: ", world.random_city().name);
// harusnya mengeluarkan value semacam "Jakarta" atau nilai random lainnya yang panjang characternya 5.
console.log(
  "Age of first citizen in another random city: ",
  world.random_city().citizens[0]
);
// harusnya mengeluarkan sebuah angka dari 0-100
console.log("# of Cities: ", world.total_cities());
// harusnya mengeluarkan angka 51
